<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Clinica Fisioterapia Guzman</title>
	<!-- CSS  -->
	<link href="<c:url value="/resources/materialize/css/materialize.min.css" />" rel="stylesheet">
	<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  -->
	<link href="<c:url value="/resources/materialize/css/materialize-icons.css" />" rel="stylesheet">
	<link href="<c:url value="/resources/css/miestilo.css" />" rel="stylesheet">
	
	<!--  Scripts-->
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/materialize/js/materialize.min.js" />"></script>
  	<script type="text/javascript" src="<c:url value="/resources/js/mijs.js" />"></script>
</head>
<body>

<!-- Barra de Navegacion -->
  <nav class="ColorBarranaVegacionCompleta white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo"><i class="large material-icons" style="color: red;">cloud</i></a>
      <ul class="hide-on-med-and-down" style="position: relative;left: 4em;">
        <li><a class="grey-text text-darken-3" href="<c:url value="/" />"><strong>INICIO</strong></a></li>
        <li><a class="grey-text text-darken-3" href="<c:url value="/Servicio" />"><strong>SERVICIOS</strong></a></li>
        <li><a class="grey-text text-darken-3" href="<c:url value="/Quienes-Somos" />"><strong>QUIENES SOMOS</strong></a></li>
        <li><a class="grey-text text-darken-3" href="<c:url value="/Contacto" />"><strong>CONTACTO</strong></a></li>
      </ul>
      <!-- Parte mobile -->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="<c:url value="/" />"><i class="small material-icons grey-text text-darken-3">dashboard</i>INICIO</a></li>
        <li><a href="<c:url value="/Servicio" />"><i class="small material-icons grey-text text-darken-3">queue</i>SERVICIOS</a></li>
        <li><a href="<c:url value="/Quienes-Somos" />"><i class="small material-icons grey-text text-darken-3">people</i>QUIENES SOMOS</a></li>
        <li><a href="<c:url value="/Contacto" />"><i class="small material-icons grey-text text-darken-3">contacts</i>CONTACTO</a></li>
      </ul>
      <!-- Parte mobile -->
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons" style="color: blue;">menu</i></a>
    </div>
  </nav>
<!-- Fin de Navegacion -->

<br><br><br><br>

<!--  -->
<div class="container z-depth-1">
    <div class="row">

        <div class="col s12 m7 l7 white">
	  		<div class="col12 s12 m12"><h5>Contacto</h5></div>
		  	<form:form method="POST" action="/spring-mvc-xml/addEmployee">
		  		
			  	<div class="input-field col s6">
		          <i class="material-icons prefix">account_circle</i>
		          <input id="icon_prefix" type="text" class="validate" name="nombrecompleto">
		          <label for="icon_prefix">Nombre Completo</label>
		        </div>
		        <div class="input-field col s6">
		          <i class="material-icons prefix">phone</i>
		          <input id="icon_telephone" type="tel" class="validate" name="telefono">
		          <label for="icon_telephone">Telefono</label>
		        </div>
		
				<div class="input-field col s12">
					<i class="material-icons prefix">contact_mail</i>
		          <input id="email" type="email" class="validate" name="correoelectornico">
		          <label for="email">Correo Electronico</label>
		        </div>	  		
		
		        <div class="input-field col s12">
		          <i class="material-icons prefix">mode_edit</i>
		          <textarea id="textarea1" class="materialize-textarea" name="mensaje"></textarea>
		          <label for="textarea1">Mensaje</label>
		        </div>	
				
				<center>
				<button class="btn waves-effect waves-light" type="submit" name="action">Enviar
				  <i class="material-icons right">send</i>
				</button>

		  	</form:form>
		  	<br>
		  	
		</div>
		
		<div class="col s12 m5 l5 blue darken-3 white-text">
	  		<div class="col12 s12 m12"><h5>Horario de Atencion</h5></div>
	  		<div class="col12 s12 m12"><h6>Lunes a Viernes: 2pm a 7pm</h6></div>
	  		<div class="col12 s12 m12"><h6>Sabados y Domingos: 8pm a 7pm</h6></div>

			<div class="col12 s12 m12"><h5>Telefonos</h5></div>
			<div class="col12 s12 m12"><h6>78892004</h5></div>
			<div class="col12 s12 m12"><h6>54131142</h5></div><br>

			<div class="col12 s12 m12"><h5>Correo Electronico</h5></div>
			<div class="col12 s12 m12"><h6>ft_gymguzman111@hotmail.com</h6></div><br>

			<div class="col12 s12 m12"><h6>Redes Sociales</h6></div>
		</div>
			
   </div>        
</div>
<!--  -->



<!-- Footer -->
<footer>
		<div class="col12 s12 m12">
          <div class="footer-copyright">
            <div class="container">
            � 2023 producido por la Familia Guzman. Creador por Daju150ti
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
		</div>
</div>
</footer>
<!-- fin Footer -->

</body>
</html>