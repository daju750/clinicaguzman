<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Clinica Fisioterapia Guzman</title>
	<!-- CSS  -->
	<link href="<c:url value="/resources/materialize/css/materialize.min.css" />" rel="stylesheet">
	<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">  -->
	<link href="<c:url value="/resources/materialize/css/materialize-icons.css" />" rel="stylesheet">
	
	<!--  Scripts-->
	<script type="text/javascript" src="<c:url value="/resources/js/jquery-3.3.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/materialize/js/materialize.min.js" />"></script>
  	<script type="text/javascript" src="<c:url value="/resources/js/mijs.js" />"></script>
</head>
<body>

<!-- Barra de Navegacion -->
  <nav class="ColorBarranaVegacionCompleta white" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo"><i class="large material-icons" style="color: red;">cloud</i></a>
      <ul class="hide-on-med-and-down" style="position: relative;left: 4em;">
        <li><a class="grey-text text-darken-3" href="<c:url value="/" />"><strong>INICIO</strong></a></li>
        <li><a class="grey-text text-darken-3" href="<c:url value="/Servicio" />"><strong>SERVICIOS</strong></a></li>
        <li><a class="grey-text text-darken-3" href="<c:url value="/Quienes-Somos" />"><strong>QUIENES SOMOS</strong></a></li>
        <li><a class="grey-text text-darken-3" href="<c:url value="/Contacto" />"><strong>CONTACTO</strong></a></li>
      </ul>
      <!-- Parte mobile -->
      <ul id="nav-mobile" class="sidenav">
        <li><a href="<c:url value="/" />"><i class="small material-icons grey-text text-darken-3">dashboard</i>INICIO</a></li>
        <li><a href="<c:url value="/Servicio" />"><i class="small material-icons grey-text text-darken-3">queue</i>SERVICIOS</a></li>
        <li><a href="<c:url value="/Quienes-Somos" />"><i class="small material-icons grey-text text-darken-3">people</i>QUIENES SOMOS</a></li>
        <li><a href="<c:url value="/Contacto" />"><i class="small material-icons grey-text text-darken-3">contacts</i>CONTACTO</a></li>
      </ul>
      <!-- Parte mobile -->
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons" style="color: blue;">menu</i></a>
    </div>
  </nav>
<!-- Fin de Navegacion -->

</body>
</html>