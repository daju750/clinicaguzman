package com.guzman.clinica.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/Quienes-Somos")
public class QuienesSomosController {

	@RequestMapping(method = RequestMethod.GET)
	public String home(Locale locale, Model model,HttpServletRequest request) {
		
		model.addAttribute("foo", "Hello Handlebars!");
		model.addAttribute("resources", request.getContextPath() + "/resources");

		return "Servicio/servicio";
	}
	
}
